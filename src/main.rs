use std::fs;
use std::io::{BufWriter, Write};
use rayon::prelude::*;

fn find_single(n1: i32, n2: i32) -> i32 {
    (n1 - n2).abs()
}


fn process(a: i32, b: i32, c: i32, d: i32, current: usize) -> usize {
    if (a == 0) && (b == 0) && (c == 0) && (d == 0) {
        current
    } else {
        process(find_single(a, b), find_single(b, c), find_single(c, d), find_single(d, a), current + 1)
    }
}

fn main() {
    // let mut save = vec![];
    // for i in 1..31 {
    //     for j in 1..31 {
    //         for k in 1..31 {
    //             for l in 1..31 {
    //                 let layers = process(i, j, k, l, 0);
    //                 let s = format!("({}, {}, {}, {})", i, j, k, l);
    //                 save.push((layers, s));
    //             }
    //         }
    //     }
    // }

    let mut save: Vec<(usize, String)> = (1..31).into_par_iter().flat_map(|i| {
        (1..31).into_par_iter().flat_map(|j| {
            (1..31).into_par_iter().flat_map(|k| {
                (1..31).into_par_iter().map(|l| {
                    let layers = process(i, j, k, l, 0);
                    let s =  format!("({}, {}, {}, {})", i, j, k, l);
                    (layers, s)
                }).collect::<Vec<(usize, String)>>()
            }).collect::<Vec<(usize, String)>>()
        }).collect::<Vec<(usize, String)>>()
    }).collect::<Vec<(usize, String)>>();

    // save.sort_by_key(|k| k.0);

    save.par_sort_by_key(|k| k.0);

    let mut file = BufWriter::new(fs::File::create("save.txt").expect("open file err"));
    for (layers, config) in save {
        writeln!(file, "{} : {}", layers, config).expect("write err");
    }
}
